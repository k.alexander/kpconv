from models.architectures import KPCNN, KPFCNN
from utils.config import Config
from datasets.Generic import *
from datasets.toronto3d import *
from torch.utils.data import DataLoader
import torch
import torch.nn as nn

model_path = "/run/media/office/Storage/git/KPConv-PyTorch/results/500 epochs/checkpoints/chkp_0500.tar"
config = Config()
config.load("./results/500 epochs/")

label_values = [0, 1, 2, 3, 4, 5, 6, 7, 8]
ignored_labels = [0]

ply_file = "/run/media/office/Storage/git/KPConv-PyTorch/data/Cassette/Cassette_GT.ply"
pred_file = ply_file.replace('.ply', '_preds.ply')

ds = GenericDataset(ply_file, config, label_name="scalar_Label", set='test', use_potentials=True)
sampler = Toronto3DSampler(ds)
net = KPFCNN(config, label_values, ignored_labels)
checkpoint = torch.load(model_path)
net.load_state_dict(checkpoint['model_state_dict'])

collate_fn = Toronto3DCollate

iterations = 10

test_loader = DataLoader(ds,
                            batch_size=1,
                            sampler=sampler,
                            collate_fn=collate_fn,
                            num_workers=config.input_threads,
                            pin_memory=True)

sampler.calibration(test_loader, verbose=True)
dev = torch.device("cuda:0")
test_smooth = 0.95
test_radius_ratio = 0.7
softmax = torch.nn.Softmax(1)
nc_model = config.num_classes
test_probs = [np.zeros((ds.input_labels[0].shape[0], nc_model))]

batch_size = len(test_loader)

for i in range(iterations):
    print("Iteration: ", i)
    for i, batch in enumerate(test_loader):
        print(f"Batch: {i}/{batch_size}")
        outputs = net(batch, config)
        stacked_probs = softmax(outputs).cpu().detach().numpy()
        s_points = batch.points[0].cpu().numpy()
        lengths = batch.lengths[0].cpu().numpy()
        in_inds = batch.input_inds.cpu().numpy()
        cloud_inds = batch.cloud_inds.cpu().numpy()
        torch.cuda.synchronize(dev)

        i0 = 0
        for b_i, length in enumerate(lengths):

            # Get prediction
            points = s_points[i0:i0 + length]
            probs = stacked_probs[i0:i0 + length]
            inds = in_inds[i0:i0 + length]
            c_i = cloud_inds[b_i]

            if 0 < test_radius_ratio < 1:
                mask = np.sum(points ** 2, axis=1) < (test_radius_ratio * config.in_radius) ** 2
                inds = inds[mask]
                probs = probs[mask]

            # Update current probs in whole cloud
            test_probs[c_i][inds] = test_smooth * test_probs[c_i][inds] + (1 - test_smooth) * probs
            i0 += length

        proj_probs = []
        for i, file_path in enumerate(test_loader.dataset.files):

            # Reproject probs on the evaluations points
            probs = test_probs[i][test_loader.dataset.test_proj[i], :]
            proj_probs += [probs]

            # Insert false columns for ignored labels
            for l_ind, label_value in enumerate(test_loader.dataset.label_values):
                if label_value in test_loader.dataset.ignored_labels:
                    proj_probs[i] = np.insert(proj_probs[i], l_ind, 0, axis=1)
                
# save cloud
points = test_loader.dataset.load_evaluation_points(ply_file)

# Get the predicted labels
preds = test_loader.dataset.label_values[np.argmax(proj_probs[i], axis=1)].astype(np.int32)

# Save plys
write_ply(pred_file,
            [points, preds],
            ['x', 'y', 'z', 'preds'])